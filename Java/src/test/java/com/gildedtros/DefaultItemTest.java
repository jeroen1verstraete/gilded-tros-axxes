package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultItemTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void testItemUpdateQuality(int sellBy, int sellByResult, int quality, int qualityResult) {
        Item[] items = new Item[]{new Item(ItemName.RING_CLEANSENING.value, sellBy, quality)};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.RING_CLEANSENING.toString(), app.items[0].name);
        assertEquals(sellByResult, app.items[0].sellIn);
        assertEquals(qualityResult, app.items[0].quality);
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                //Quality degrades
                Arguments.of(1, 0, 10, 9),
                //Quality degrades twice as fast after sell by date has passed
                Arguments.of(0, -1, 10, 8),
                Arguments.of(-1, -2, 10, 8),
                //Quality cannot go below 0
                Arguments.of(1, 0, 1, 0),
                Arguments.of(0, -1, 1, 0),
                Arguments.of(-1, -2, 1, 0),
                Arguments.of(1, 0, 0, 0),
                Arguments.of(0, -1, 0, 0),
                Arguments.of(-1, -2, 0, 0),

                //The quality of an item is never more than 50
                Arguments.of(1, 0, 55, 49),
                Arguments.of(0, -1, 55, 48),
                Arguments.of(-1, -2, 55, 48)
        );
    }
}
