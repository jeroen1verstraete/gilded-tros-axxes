package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LegendaryItemTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void testLegendaryUpdateQuality(int sellBy, int sellByResult, int quality, int qualityResult) {
        Item[] items = new Item[]{new Item(ItemName.KEYCHAIN.value, sellBy, quality)};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.KEYCHAIN.toString(), app.items[0].name);
        assertEquals(sellByResult, app.items[0].sellIn);
        assertEquals(qualityResult, app.items[0].quality);
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                //Legendary items don't age
                Arguments.of(1, 1, 80,80),
                Arguments.of(0, 0, 80, 80),
                Arguments.of(-1, -1, 80, 80),

                //Legendary items should have a quality of 80
                Arguments.of(1, 1, 60,80),
                Arguments.of(1, 1, 100,80),

                Arguments.of(-1, -1, 60, 80),
                Arguments.of(-1, -1, 100, 80)
        );
    }
}
