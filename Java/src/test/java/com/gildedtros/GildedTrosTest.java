package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedTrosTest {

    @Test
    void testItemName() {
        Item[] items = new Item[] { new Item(ItemName.RING_CLEANSENING.value, 0, 0) };
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.RING_CLEANSENING.toString(), app.items[0].name);
    }

}
