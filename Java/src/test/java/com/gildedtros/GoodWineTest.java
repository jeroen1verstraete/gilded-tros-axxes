package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GoodWineTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void testWineUpdateQuality(int sellBy, int sellByResult, int quality, int qualityResult) {
        Item[] items = new Item[]{new Item(ItemName.GOOD_WINE.value, sellBy, quality)};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.GOOD_WINE.toString(), app.items[0].name);
        assertEquals(sellByResult, app.items[0].sellIn);
        assertEquals(qualityResult, app.items[0].quality);
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                //Wine becomes better with age
                Arguments.of(1, 0, 10, 11),
                Arguments.of(0, -1, 10, 12),
                Arguments.of(-1, -2, 10, 12),
                Arguments.of(1, 0, 0, 1),
                Arguments.of(0, -1, 0, 2),
                Arguments.of(-1, -2, 0, 2),
                //Maximum quality of 50
                Arguments.of(1, 0, 49, 50),
                Arguments.of(0, -1, 49, 50),
                Arguments.of(-1, -2, 49, 50),
                Arguments.of(1, 0, 50, 50),
                Arguments.of(0, -1, 50, 50),
                Arguments.of(-1, -2, 50, 50),

                //The quality of an item is never more than 50
                Arguments.of(1, 0, 55, 50),
                Arguments.of(0, -1, 55, 50),
                Arguments.of(-1, -2, 55, 50)
        );
    }
}
