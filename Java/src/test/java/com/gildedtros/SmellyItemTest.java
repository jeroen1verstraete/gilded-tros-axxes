package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SmellyItemTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void testSmellyItemQuality(int sellBy, int sellByResult, int quality, int qualityResult) {
        Item[] items = new Item[]{new Item(ItemName.DUPLICATE_CODE.value, sellBy, quality)};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.DUPLICATE_CODE.toString(), app.items[0].name);
        assertEquals(sellByResult, app.items[0].sellIn);
        assertEquals(qualityResult, app.items[0].quality);
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                //Quality degrades
                Arguments.of(1, 0, 10, 8),
                //Quality degrades twice as fast after sell by date has passed
                Arguments.of(0, -1, 10, 6),
                Arguments.of(-1, -2, 10, 6),
                //Quality cannot go below 0
                Arguments.of(1, 0, 1, 0),
                Arguments.of(0, -1, 3, 0),
                Arguments.of(-1, -2, 3, 0),
                Arguments.of(1, 0, 0, 0),
                Arguments.of(0, -1, 0, 0),
                Arguments.of(-1, -2, 0, 0)
        );
    }
}
