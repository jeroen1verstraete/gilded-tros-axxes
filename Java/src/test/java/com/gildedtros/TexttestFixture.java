package com.gildedtros;

import com.gildedtros.item_names.ItemName;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("AXXES CODE KATA - GILDED TROS");

        Item[] items = new Item[] {
                new Item(ItemName.RING_CLEANSENING.value, 10, 20),
                new Item(ItemName.GOOD_WINE.value, 2, 0),
                new Item(ItemName.ELIXIR_SOLID.value, 5, 7),
                new Item(ItemName.KEYCHAIN.value, 0, 80),
                new Item(ItemName.KEYCHAIN.value, -1, 80),
                new Item(ItemName.RE_FACTOR.value, 15, 20),
                new Item(ItemName.RE_FACTOR.value, 10, 49),
                new Item(ItemName.HAXX.value, 5, 49),
                // these smelly items do not work properly yet
                new Item(ItemName.DUPLICATE_CODE.value, 3, 6),
                new Item(ItemName.LONG_METHODS.value, 3, 6),
                new Item(ItemName.UGLY_VARIABLES.value, 3, 6) };

        GildedTros app = new GildedTros(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }

}
