package com.gildedtros;

import com.gildedtros.item_names.ItemName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BackstagePassTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void testBackstagePassUpdateQuality(int sellBy, int sellByResult, int quality, int qualityResult) {
        Item[] items = new Item[]{new Item(ItemName.RE_FACTOR.value, sellBy, quality)};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(ItemName.RE_FACTOR.toString(), app.items[0].name);
        assertEquals(sellByResult, app.items[0].sellIn);
        assertEquals(qualityResult, app.items[0].quality);
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                //Backstage passes increase in quality
                Arguments.of(50, 49, 10, 11),
                Arguments.of(50, 49, 50,50),

                //Backstage passes increase twice as fast in quality < 10 days
                Arguments.of(10, 9, 10, 12),
                Arguments.of(10, 9, 49, 50),
                Arguments.of(10, 9, 50,50),

                //Backstage passes increase thrice as fast in quality < 5 days
                Arguments.of(1, 0, 10, 13),
                Arguments.of(5, 4, 10, 13),
                Arguments.of(5,4, 48, 50),
                Arguments.of(5,4, 49, 50),
                Arguments.of(5,4, 50, 50),

                //Backstage passes lose quality after event
                Arguments.of(0, -1, 10, 0),
                Arguments.of(-1,-2, 10, 0),

                //Quality never below 0
                Arguments.of(0, -1, 0, 0),
                Arguments.of(-1,-2, 0, 0),

                //The quality of an item is never more than 50
                Arguments.of(1, 0, 55, 50),
                Arguments.of(0, -1, 55, 0),
                Arguments.of(-1, -2, 55, 0)
        );
    }
}
