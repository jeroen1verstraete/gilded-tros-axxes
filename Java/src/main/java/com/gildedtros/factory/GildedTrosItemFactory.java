package com.gildedtros.factory;

import com.gildedtros.Item;
import com.gildedtros.item_names.ItemName;
import com.gildedtros.item_types.BackstagePass;
import com.gildedtros.item_types.DefaultItem;
import com.gildedtros.item_types.GildedTrosItemInterface;
import com.gildedtros.item_types.GoodWineItem;
import com.gildedtros.item_types.LegendaryItem;
import com.gildedtros.item_types.SmellyItem;

public class GildedTrosItemFactory {
    private GildedTrosItemFactory() {
        throw new IllegalStateException("Utility class");
    }

    public static GildedTrosItemInterface getGildedTrosItem(Item item) {
        ItemName itemName = ItemName.fromString(item.name);
        GildedTrosItemInterface gildedTrosItem;
        switch (itemName.type) {
            case LEGENDARY:
                gildedTrosItem = new LegendaryItem(item);
                break;
            case GOOD_WINE:
                gildedTrosItem = new GoodWineItem(item);
                break;
            case BACKSTAGE_PASS:
                gildedTrosItem = new BackstagePass(item);
                break;
            case SMELLY:
                gildedTrosItem = new SmellyItem(item);
                break;
            default:
                gildedTrosItem = new DefaultItem(item);
        }
        return gildedTrosItem;
    }
}
