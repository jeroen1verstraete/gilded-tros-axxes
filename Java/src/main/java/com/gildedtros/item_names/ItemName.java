package com.gildedtros.item_names;

public enum ItemName {
    RING_CLEANSENING("Ring of Cleansening Code", ItemType.DEFAULT),
    ELIXIR_SOLID("Elixir of the SOLID", ItemType.DEFAULT),
    GOOD_WINE("Good Wine", ItemType.GOOD_WINE),
    RE_FACTOR("Backstage passes for Re:Factor", ItemType.BACKSTAGE_PASS),
    HAXX("Backstage passes for HAXX", ItemType.BACKSTAGE_PASS),
    KEYCHAIN("B-DAWG Keychain", ItemType.LEGENDARY),
    DUPLICATE_CODE("Duplicate Code", ItemType.SMELLY),
    LONG_METHODS("Long Methods", ItemType.SMELLY),
    UGLY_VARIABLES("Ugly Variable Names", ItemType.SMELLY);


    public final String value;
    public final ItemType type;

    ItemName(String value, ItemType type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public String toString() {
        return this.value;
    }


    public static ItemName fromString(String text) {
        for (ItemName name : ItemName.values()) {
            if (name.value.equalsIgnoreCase(text)) {
                return name;
            }
        }
        throw new IllegalArgumentException(String.format("No ItemName with value: %s", text));
    }

}
