package com.gildedtros.item_names;

public enum ItemType {
    DEFAULT,
    LEGENDARY,
    SMELLY,
    GOOD_WINE,
    BACKSTAGE_PASS;
}
