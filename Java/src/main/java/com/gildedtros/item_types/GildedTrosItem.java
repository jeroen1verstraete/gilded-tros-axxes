package com.gildedtros.item_types;


import com.gildedtros.Item;

abstract class GildedTrosItem implements GildedTrosItemInterface {
    static final int MAX_QUALITY = 50;
    Item item;

    protected GildedTrosItem(Item item) {
        if (item.quality > MAX_QUALITY) {
            item.quality = MAX_QUALITY;
        }
        this.item = item;
    }

    public final void nextDay() {
        updateSellBy();
        updateQuality();
    }

    protected void updateQuality() {
        if (this.item.quality > 0) {
            this.item.quality--;
        }

        if (this.item.sellIn < 0 && this.item.quality > 0) {
            this.item.quality--;
        }
    }

    protected void updateSellBy() {
        this.item.sellIn--;
    }
}
