package com.gildedtros.item_types;

import com.gildedtros.Item;

public class BackstagePass extends GildedTrosItem {
    public BackstagePass(Item item) {
        super(item);
    }

    @Override
    protected void updateQuality() {
        if (this.item.quality < MAX_QUALITY) {
            this.item.quality++;

            if (this.item.sellIn < 11 && this.item.quality < MAX_QUALITY) {
                this.item.quality++;
            }

            if (this.item.sellIn < 6 && this.item.quality < MAX_QUALITY) {
                this.item.quality++;
            }
        }

        if (this.item.sellIn < 0) {
            this.item.quality = 0;
        }
    }
}
