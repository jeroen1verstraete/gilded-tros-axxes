package com.gildedtros.item_types;

import com.gildedtros.Item;

public class LegendaryItem extends GildedTrosItem {
    static final int MAX_QUALITY = 80;

    public LegendaryItem(Item item) {
        super(item);
        item.quality = MAX_QUALITY;
    }

    @Override
    protected void updateQuality() {
//        Quality never changes
    }

    @Override
    protected void updateSellBy() {
//        SellBy never changes
    }
}
