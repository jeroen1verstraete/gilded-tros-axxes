package com.gildedtros;

import com.gildedtros.factory.GildedTrosItemFactory;
import com.gildedtros.item_types.GildedTrosItemInterface;

import java.util.Arrays;

class GildedTros {
    Item[] items;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Arrays.stream(items)
                .map(GildedTrosItemFactory::getGildedTrosItem)
                .forEach(GildedTrosItemInterface::nextDay);
    }

}